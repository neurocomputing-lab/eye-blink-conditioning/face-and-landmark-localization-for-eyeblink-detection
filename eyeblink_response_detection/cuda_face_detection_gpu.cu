// FOR A DETAILED EXPLANATION OF THE CODE, READ CHAPTER 4 AND 5 OF THE THESIS REPORT "REAL-TIME FACE 
// AND LANDMARK LOCALIZATION FOR EYEBLINK-RESPONSE DETECTION"
// THE FACE DETECTION ALGORITHM IS BASED ON THE HISTOGRAM OF GRADIENTS ALGORITHM DESCRIBED IN 
// "OBJECT DETECTION WITH DISCRIMINATIVELY TRAINED PART BASED MODELS" BY F. FELZENZWALB 

#include <sys/time.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include "dlib/image_processing/scan_fhog_pyramid.h" 
#include "dlib/image_processing/frontal_face_detector_abstract.h"
#include "dlib/image_processing/object_detector.h"
#include "dlib/image_processing/shape_predictor.h"
#include <cuda_profiler_api.h>
#include <dlib/array2d.h>
#include <omp.h>

using std::cout;
using std::cerr;
using std::endl;
using std::fixed;
using std::set;

typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<6>,dlib::default_fhog_feature_extractor>::fhog_filterbank fhog_filterbank;

//cumulative filter numbers, constant memory cannot be allocated dynamically; (61+58+54+66+66)*10 for all 5 face direction filters
__constant__ float dev_const_col_filters[3050]; 
__constant__ float dev_const_row_filters[3050]; 

 //32*5 for all starts and ends per feature
__constant__ unsigned int dev_const_feature_table[160];

__constant__ float dev_const_threshs[5];
__constant__ int dev_cum_no_filters[5];
__constant__ int dev_const_width_imgs[MAX_NO_SCALES];
__constant__ int dev_const_height_imgs[MAX_NO_SCALES];
__constant__ int dev_const_pitch_imgs[MAX_NO_SCALES];
__constant__ int dev_const_width_cells_imgs[MAX_NO_SCALES];
__constant__ int dev_const_height_cells_imgs[MAX_NO_SCALES]; 
__constant__ int dev_const_width_cells_hists[MAX_NO_SCALES];
__constant__ int dev_const_height_cells_hists[MAX_NO_SCALES]; 
__constant__ int dev_const_width_cells_features[MAX_NO_SCALES];
__constant__ int dev_const_height_cells_features[MAX_NO_SCALES];
__constant__ int dev_const_pitch_norms[MAX_NO_SCALES];
__constant__ int dev_const_first_row;
__constant__ int dev_const_first_col;
__constant__ int dev_const_last_rows[MAX_NO_SCALES];
__constant__ int dev_const_last_cols[MAX_NO_SCALES];
__constant__ float dev_const_y_scales[MAX_NO_SCALES];
__constant__ float dev_const_x_scales[MAX_NO_SCALES];
__constant__ int dev_const_padding_rows_offset;
__constant__ int dev_const_padding_cols_offset;
__constant__ int dev_const_filter_rows_padding;
__constant__ int dev_const_filter_cols_padding;
__constant__ int dev_const_width_grad_imgs[MAX_NO_SCALES];
__constant__ int dev_const_height_grad_imgs[MAX_NO_SCALES];

// All arrays needed for the intermediate feature representations of the image and classification steps are stored in 
// one single large array to reduce malloc and memset overhead. The dev_const_start_ptrs array contains the starting 
// points of each "original" array in the large combined array    
__constant__ float* dev_const_big_pointer[NO_THREADS]; 
__constant__ int dev_const_start_ptrs[MAX_NO_SCALES*(3+NO_FILTERS)+2];
__constant__ unsigned char* dev_const_imgs[NO_THREADS*MAX_NO_SCALES];

cudaStream_t streams[MAX_NO_SCALES];
float* big_pointer[NO_THREADS];
int size_memory_memset_float;
std::vector<int> start_pointers_vec;
unsigned char* dev_imgs[NO_THREADS*MAX_NO_SCALES]; 
int width_imgs[MAX_NO_SCALES]; 
int height_imgs[MAX_NO_SCALES]; 
int pitch_imgs[MAX_NO_SCALES];
int width_cells_imgs[MAX_NO_SCALES];
int height_cells_imgs[MAX_NO_SCALES];
int width_cells_hists[MAX_NO_SCALES]; 
int height_cells_hists[MAX_NO_SCALES];
int width_cells_features[MAX_NO_SCALES]; 
int height_cells_features[MAX_NO_SCALES]; 
int pitch_norms[MAX_NO_SCALES];
int first_row; 
int first_col;
int last_rows[MAX_NO_SCALES];
int last_cols[MAX_NO_SCALES];
float y_scales[MAX_NO_SCALES];
float x_scales[MAX_NO_SCALES];
int padding_rows_offset;
int padding_cols_offset;
int filter_rows_padding;
int filter_cols_padding; 
int width_grad_imgs[MAX_NO_SCALES];
int height_grad_imgs[MAX_NO_SCALES];

#define checkCudaCall(result) {                                     \
if (result != cudaSuccess){                                     \
  cerr << "cuda error: " << cudaGetErrorString(result);       \
  cerr << " in " << __FILE__ << " at line "<< __LINE__<<endl; \
  exit(1);                                                    \
}                                                               \
}

void initialize_profiler()
{
    //exclude first cudamalloc in profiler that apparantly takes up a lot of initialization time..
  unsigned char* temp;
  checkCudaCall(cudaMalloc(&temp, sizeof(unsigned char)));
  checkCudaCall(cudaFree(temp));
  cudaProfilerStart();
}

void end_profiler()
{
  cudaProfilerStop();
}

// Kernel that multiplies the features of each area of 10 x 10 cells with weights and stores 
// the accumulated result in the cell that represents the middle of that area
__global__ void classificationFilterCudaKernel(const int scale, const int no_scales) 
{
  int cell_x = blockIdx.x*blockDim.x + threadIdx.x; 
  const int cell_y = blockIdx.y*(blockDim.y-10) + threadIdx.y; //moet dit niet negen zijn
  const int feature_number = blockIdx.z*blockDim.z + threadIdx.z;

  if (cell_x<(dev_const_last_cols[scale]-dev_const_first_col)*NO_THREADS && cell_y < dev_const_height_cells_features[scale] && feature_number<NO_FEATURES)
  {
    int img = cell_x/(dev_const_last_cols[scale]-dev_const_first_col);
    cell_x = cell_x%(dev_const_last_cols[scale]-dev_const_first_col);

    __shared__ float smem[32][32]; 
    const int linear_featurenumber =  (feature_number*dev_const_height_cells_features[scale]+cell_y)*dev_const_width_cells_features[scale]+cell_x;     

    float features[10];

    for(int i=0;i<10;++i)
    {
      features[i]=dev_const_big_pointer[img][dev_const_start_ptrs[no_scales*2+scale]+linear_featurenumber+i];
    }

    #pragma unroll            
    for(int fb=0;fb<5;++fb)
    {
      const int start_filternumber = dev_const_feature_table[fb*32+feature_number]; 
      const int end_filternumber = dev_const_feature_table[fb*32+feature_number+1]; 
      #pragma unroll           
      for(int filter_number=start_filternumber;filter_number<end_filternumber;++filter_number)
      {
        int cum_filternumber = (dev_cum_no_filters[fb]+filter_number)*10/2;
        float saliency_feature=0; 

        float2* row_filters_p2 = reinterpret_cast<float2*>(dev_const_row_filters);
        float2* col_filters_p2 = reinterpret_cast<float2*>(dev_const_col_filters);

        // row filters
        #pragma unroll            
        for (int n =0; n < 5; ++n)
        {
          float2 row_filters_2 = row_filters_p2[cum_filternumber+n];
          saliency_feature += (features[2*n]*row_filters_2.x + features[2*n+1]*row_filters_2.y);
        }

        smem[threadIdx.y][threadIdx.x]=saliency_feature;

        __syncthreads();

        // col filters        
        if(threadIdx.y<22)
        {
          float scratch_image[10];
          #pragma unroll            
          for (int n =0; n < 10; ++n)
          {
            scratch_image[n] = smem[threadIdx.y+n][threadIdx.x];
          }
          saliency_feature = 0;
          #pragma unroll
          for (int n =0; n < 5; ++n)
          {
            float2 col_filters_2 = col_filters_p2[cum_filternumber+n];
            saliency_feature += scratch_image[2*n]*col_filters_2.x + scratch_image[2*n+1]*col_filters_2.y;
          }
          atomicAdd(&(dev_const_big_pointer[img][dev_const_start_ptrs[no_scales*3+scale*NO_FILTERS+fb]+cell_y*dev_const_width_cells_features[scale]+cell_x]),saliency_feature);
        }
      }
    }
  }
}

// Kernel that compares the face score of each cell in the saliency images (for each of the 5 head rotations 
// detectors) with a threshold and stores cell location, detection score, image scale and the rotation-number of the 
// detector in case the face score is higher than the threshold.
__global__ void detectionThreshCudaKernel(const int scale, const int no_scales) 
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  int img =  blockIdx.z*blockDim.z + threadIdx.z;

  if(x<(dev_const_last_cols[scale]-dev_const_first_col) && y<(dev_const_last_rows[scale]-dev_const_first_row))
  {
    for(int fb_num=0;fb_num<NO_FILTERS;++fb_num)
    {
      float score = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales*3+scale*NO_FILTERS+fb_num]+y*dev_const_width_cells_features[scale]+x];
      if(score>=dev_const_threshs[fb_num])
      {
        float write_location = atomicAdd(&dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img+1]],1.0f);
        dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img]+5*(int)write_location]   = float(x+dev_const_first_col);
        dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img]+5*(int)write_location+1] = float(y+dev_const_first_row);
        dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img]+5*(int)write_location+2] = score;
        dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img]+5*(int)write_location+3] = float(scale);
        dev_const_big_pointer[0][dev_const_start_ptrs[no_scales*(3+NO_FILTERS)+2*img]+5*(int)write_location+4] = float(fb_num);
      }
    }
  }
}

// Host code from where the classification and detection kernels are launched
void classify_regions_cuda(const int no_scales)
{
  dim3 dimBlock(32, 32, 1);

  for(int i=BEGIN_SCALE;i<no_scales;i++)
  {
    dim3 dimGrid(((last_cols[i]-first_col)*NO_THREADS + dimBlock.x - 1) / dimBlock.x, (height_cells_features[i] + (dimBlock.y-10) - 1) / (dimBlock.y-10), (NO_FEATURES + dimBlock.z - 1) / dimBlock.z);
    classificationFilterCudaKernel<<<dimGrid, dimBlock, 0, streams[i]>>>(i, no_scales);     
  }

  for(int i=BEGIN_SCALE;i<no_scales;i++)
  {
    dim3 dimGrid(((last_cols[i]-first_col)  + dimBlock.x - 1) / dimBlock.x, ((last_rows[i]-first_row) + dimBlock.y - 1) / dimBlock.y, (NO_THREADS + dimBlock.z -1) / dimBlock.z);
    detectionThreshCudaKernel<<<dimGrid, dimBlock, 0, streams[i]>>>(i, no_scales);
  }     
}

// Kernel in which the image is downscaled
__global__ void bilinearInterpolateCudaKernel(const int scale, const int no_scales) 
{

  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  int img =  blockIdx.z*blockDim.z + threadIdx.z;

  if (x< dev_const_width_imgs[scale] && y < dev_const_height_imgs[scale] && img<NO_THREADS)
  {   
    float y_new = y*dev_const_y_scales[scale];
    int top = floor(y_new);
    int bottom = min(top+1,dev_const_height_imgs[scale-1]-1);
    float tb_frac = y_new-top;

    float x_new = x*dev_const_x_scales[scale];
    int left = floor(x_new);
    int right = (min(left+1,dev_const_width_imgs[scale-1]-1));
    float lr_frac = x_new-left;

    const int size_of_in_image = dev_const_pitch_imgs[scale-1]*dev_const_height_imgs[scale-1];
    unsigned int tl = static_cast<unsigned int>(dev_const_imgs[scale-1][img*size_of_in_image+top*dev_const_pitch_imgs[scale-1]+left]);
    unsigned int tr = static_cast<unsigned int>(dev_const_imgs[scale-1][img*size_of_in_image+top*dev_const_pitch_imgs[scale-1]+right]);
    unsigned int bl = static_cast<unsigned int>(dev_const_imgs[scale-1][img*size_of_in_image+bottom*dev_const_pitch_imgs[scale-1]+left]);
    unsigned int br = static_cast<unsigned int>(dev_const_imgs[scale-1][img*size_of_in_image+bottom*dev_const_pitch_imgs[scale-1]+right]);

    const int size_of_out_image = dev_const_pitch_imgs[scale]*dev_const_height_imgs[scale];
    dev_const_imgs[scale][img*size_of_out_image+y*dev_const_pitch_imgs[scale]+x] = static_cast<unsigned char>((1-tb_frac)*((1-lr_frac)*tl
      + lr_frac*tr) + tb_frac*((1-lr_frac)*bl
      + lr_frac*br));
  }
}

// Host code from which the bilinearInterpolateCudaKernel (downscaling) kernel is launched
void resize_image_cuda(const int no_scales)
{
  dim3 dimBlock(32, 32, 1);

  for(int i=1;i<no_scales;i++)
  {
    dim3 dimGrid((width_imgs[i]  + dimBlock.x - 1) / dimBlock.x, (height_imgs[i] + dimBlock.y - 1) / dimBlock.y, (NO_THREADS + dimBlock.z -1) / dimBlock.z);
    bilinearInterpolateCudaKernel<<<dimGrid, dimBlock>>>(i, no_scales);
  }   
}

// Kernel that compues the gradient direction and magnitude of each pixel and contributes to the 
// histograms of four surrounding cells of that pixel
__global__ void gradientHistogramCudaKernel(const int scale, const int no_scales) 
{
  int x = blockIdx.x*blockDim.x + threadIdx.x+4;
  int y = blockIdx.y*blockDim.y + threadIdx.y+4;
  int img =  blockIdx.z*blockDim.z + threadIdx.z;

  if (x <= (dev_const_width_grad_imgs[scale]) && y <= (dev_const_height_grad_imgs[scale]) && img<NO_THREADS)
  { 
    const int size_of_image = dev_const_pitch_imgs[scale]*dev_const_height_imgs[scale];  
    const float grad_x = static_cast< float >((dev_const_imgs[scale][img * size_of_image + y * dev_const_pitch_imgs[scale] + x+1]) - dev_const_imgs[scale][img * size_of_image + y * dev_const_pitch_imgs[scale] + x-1]);
    const float grad_y = static_cast< float >((dev_const_imgs[scale][img * size_of_image + (y+1) * dev_const_pitch_imgs[scale] + x]) - dev_const_imgs[scale][img * size_of_image + (y-1) * dev_const_pitch_imgs[scale] + x]);
    const float grad_mag = sqrt(grad_y*grad_y+grad_x*grad_x);
    const float directions[18] = {1.0000, 0.0000, 0.9397, 0.3420, 0.7660, 0.6428, 0.500, 0.8660, 0.1736, 
      0.9848, -0.1736, 0.9848, -0.5000, 0.8660, -0.7660, 0.6428, -0.9397, 0.3420};

      float best_dot = 0;
      int best_o = 0;

      for (int o = 0; o < 9; o++)
      {
        float dot = grad_x*directions[2*o] + grad_y*directions[2*o+1];
        if(dot>best_dot){
          best_dot=dot;
          best_o=o;
        }
        else if(-dot > best_dot){
          best_dot=-dot;
          best_o=o+9;
        }
      }

      int cell_size=8;

      float yp_sm = (threadIdx.y+4+0.5f)/cell_size - 0.5f;
      int iyp_sm = (int)(yp_sm);
      float vy0_sm = yp_sm - iyp_sm;
      float vy1_sm = 1.0f - vy0_sm;

      float xp_sm  = (threadIdx.x+4+0.5f)/cell_size + 0.5f;
      int   ixp_sm = int(xp_sm);
      float vx0_sm = xp_sm - ixp_sm;
      float vx1_sm = 1.0f - vx0_sm;

      vx1_sm *= grad_mag;
      vx0_sm *= grad_mag;

    //conversion to "fixed point" representation to make use of hadware native shared-memory
    // atomic operations
    int v11_sm = (int)(vy1_sm*vx1_sm*10000); 
    int v01_sm = (int)(vy0_sm*vx1_sm*10000);
    int v10_sm = (int)(vy1_sm*vx0_sm*10000);
    int v00_sm = (int)(vy0_sm*vx0_sm*10000);

    //SHARED MEMORY
    int t = threadIdx.x + threadIdx.y * blockDim.x;
    int nt = blockDim.x * blockDim.y;

    int width_sm = ceilf(blockDim.x/cell_size+1); 
    int height_sm = ceilf(blockDim.y/cell_size+1); 

    // Boundary checks
    if(blockIdx.x==(gridDim.x-1))
    {
      width_sm = ceilf((float(dev_const_width_grad_imgs[scale]) - blockIdx.x*blockDim.x)/cell_size+1);
    }
    if(blockIdx.y==(gridDim.y-1))
    {
      height_sm = ceilf((float(dev_const_height_grad_imgs[scale]) - blockIdx.y*blockDim.y)/cell_size+1);
    }

    __shared__ int smem[18*5*5];

    //MEMSET 0
    for (int i = t; i < NO_BINS*5*5; i += nt)
    {
      smem[i] = 0;
    }
    __syncthreads();

    // Combine results in shared memory
    atomicAdd(&smem[(((iyp_sm))*width_sm+(ixp_sm-1))*NO_BINS+best_o], v11_sm);
    atomicAdd(&smem[(((iyp_sm+1))*width_sm+(ixp_sm-1))*NO_BINS+best_o], v01_sm);
    atomicAdd(&smem[(((iyp_sm))*width_sm+((ixp_sm)))*NO_BINS+best_o], v10_sm);
    atomicAdd(&smem[(((iyp_sm+1))*width_sm+((ixp_sm)))*NO_BINS+best_o], v00_sm);

    __syncthreads();

    int iyp_start = (int)((blockIdx.y*blockDim.y+4.5f)/cell_size + 0.5f);
    int ixp_start = (int)((blockIdx.x*blockDim.x+4.5f)/cell_size + 0.5f);
    int starting_point = (((iyp_start)*dev_const_width_cells_hists[scale]+ixp_start)*NO_BINS);

    // Write to global memory
    if(t<NO_BINS*width_sm*height_sm)
    {
      int row = floor((float)t/(float)(NO_BINS*width_sm));
      int column = t%(NO_BINS*height_sm);
      int write_location = dev_const_start_ptrs[scale] + starting_point + row*dev_const_width_cells_hists[scale]*NO_BINS+column;
      atomicAdd(&dev_const_big_pointer[img][write_location],float((float)smem[t]/10000.0f));  //hoeft volgens mij allebei niet die floats omdat 100000.0f             
    }      
  }
}


// Compute the energy of each cell, used later for energy features and cell-histogram normalization
__global__ void energyCudaKernel(const int scale, const int no_scales) 
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  int img =  blockIdx.z*blockDim.z + threadIdx.z;

  if (x< dev_const_width_cells_imgs[scale] && y < dev_const_height_cells_imgs[scale] && img<NO_THREADS)
  { 
    float norm = 0;
    int linear_cellnumber_hist = (((y + 1)*dev_const_width_cells_hists[scale])+(x+1))*NO_BINS; 
    for (int o = 0; o < 9; o++) 
    {
      norm += (dev_const_big_pointer[img][dev_const_start_ptrs[scale]+linear_cellnumber_hist+o]+dev_const_big_pointer[img][dev_const_start_ptrs[scale]+linear_cellnumber_hist+o+9])*
      (dev_const_big_pointer[img][dev_const_start_ptrs[scale]+linear_cellnumber_hist+o]+dev_const_big_pointer[img][dev_const_start_ptrs[scale]+linear_cellnumber_hist+o+9]);
    }
    dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+y*dev_const_width_cells_imgs[scale]+x] = norm;
  }
}

// Compute the final 31 features per cell from the 18 histogram bins
__global__ void featuresCudaKernel(const int scale, const int no_scales) 
{
  int x = blockIdx.x*blockDim.x + threadIdx.x;
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  int img =  blockIdx.z*blockDim.z + threadIdx.z;

  if (x< (dev_const_width_cells_features[scale]-(dev_const_filter_cols_padding-1)) && y < (dev_const_height_cells_features[scale]-(dev_const_filter_rows_padding-1)) && img<NO_THREADS)
  {
    int linear_cellnumber_hist = (((y + 1 + 1)*dev_const_width_cells_hists[scale])+(x+1 +1))*NO_BINS;
    int linear_featurenumber = ((y+dev_const_padding_rows_offset)*dev_const_width_cells_features[scale])+(x+dev_const_padding_cols_offset);

    const float eps = 0.0001;
    float norm00 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y)*dev_const_width_cells_imgs[scale]+x]; 
    float norm01 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y)*dev_const_width_cells_imgs[scale]+x+1];
    float norm02 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y)*dev_const_width_cells_imgs[scale]+x+2];
    float norm10 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+1)*dev_const_width_cells_imgs[scale]+x];
    float norm11 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+1)*dev_const_width_cells_imgs[scale]+x+1];
    float norm12 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+1)*dev_const_width_cells_imgs[scale]+x+2];
    float norm20 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+2)*dev_const_width_cells_imgs[scale]+x];
    float norm21 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+2)*dev_const_width_cells_imgs[scale]+x+1];
    float norm22 = dev_const_big_pointer[img][dev_const_start_ptrs[no_scales+scale]+(y+2)*dev_const_width_cells_imgs[scale]+x+2];

    float nn1 = 0.2f*sqrt(norm11+norm12+norm21+norm22+eps);
    float nn2 = 0.2f*sqrt(norm01+norm02+norm11+norm12+eps);
    float nn3 = 0.2f*sqrt(norm10+norm11+norm20+norm21+eps);
    float nn4 = 0.2f*sqrt(norm00+norm01+norm10+norm11+eps);

    float n1 = 0.1f/nn1; 
    float n2 = 0.1f/nn2;
    float n3 = 0.1f/nn3;
    float n4 = 0.1f/nn4;

    float t1 = 0;
    float t2 = 0;
    float t3 = 0;
    float t4 = 0;

    float h01;
    float h02;
    float h03;
    float h04;

    float temp0;
    float hist[18];

    for(int i=0;i<18;i++)
    {
      hist[i]=dev_const_big_pointer[img][dev_const_start_ptrs[scale]+linear_cellnumber_hist+i];
    }

    for (int o = 0; o < 18; o++) 
    {
      // Histogram values are normalized
      temp0 = hist[o];
      h01 = min(temp0,nn1)*n1;
      h02 = min(temp0,nn2)*n2;
      h03 = min(temp0,nn3)*n3;
      h04 = min(temp0,nn4)*n4;

      // Store the first 18 features based on normalized signed histogram values of the cell
      dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+o*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = h01+h02+h03+h04;
      t1 += h01;
      t2 += h02;
      t3 += h03;
      t4 += h04;
    }

    t1 *= 2*0.2357f;
    t2 *= 2*0.2357f;
    t3 *= 2*0.2357f;
    t4 *= 2*0.2357f;

    for (int o = 0; o < 9; o++) 
    {
      temp0 = hist[o] + hist[o+9];
      h01 = min(temp0,nn1)*n1;
      h02 = min(temp0,nn2)*n2;
      h03 = min(temp0,nn3)*n3;
      h04 = min(temp0,nn4)*n4;
      // Store 9 features based on normalized unsigned histogram values of the cell
      dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+(o+18)*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = h01+h02+h03+h04;
    }

    // Store the final 4 features based on energy features of surrounding cells
    dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+27*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = t1;
    dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+28*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = t2;
    dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+29*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = t3;
    dev_const_big_pointer[img][dev_const_start_ptrs[2*no_scales+scale]+linear_featurenumber+30*dev_const_height_cells_features[scale]*dev_const_width_cells_features[scale]] = t4;
  }
}

// Host code from where the feature extraction kernels are launched
void compute_features_cuda(const int no_scales)
{
  dim3 dimBlock(32, 32, 1);

  for(int i=BEGIN_SCALE;i<no_scales;i++)
  {
    dim3 dimGrid((width_grad_imgs[i]  + dimBlock.x - 1) / dimBlock.x, (height_grad_imgs[i] + dimBlock.y - 1) / dimBlock.y, (NO_THREADS + dimBlock.z -1) / dimBlock.z);
    gradientHistogramCudaKernel<<<dimGrid, dimBlock, 0, streams[i]>>>(i, no_scales);
  }

  dimBlock.x = 16;
  dimBlock.y = 8;
  dimBlock.z = 1;

  for(int i=BEGIN_SCALE;i<no_scales;i++)
  {
    dim3 dimGrid((width_cells_imgs[i]  + dimBlock.x - 1) / dimBlock.x, (height_cells_imgs[i] + dimBlock.y - 1) / dimBlock.y, (NO_THREADS + dimBlock.z -1) / dimBlock.z);
    energyCudaKernel<<<dimGrid, dimBlock, 0, streams[i]>>>(i, no_scales);
  }

  for(int i=BEGIN_SCALE;i<no_scales;i++)
  {  
    dim3 dimGrid(((width_cells_features[i]-(filter_cols_padding-1))  + dimBlock.x - 1) / dimBlock.x, (height_cells_features[i]-(filter_rows_padding-1) + dimBlock.y - 1) / dimBlock.y, (NO_THREADS + dimBlock.z -1) / dimBlock.z);      
    featuresCudaKernel<<<dimGrid, dimBlock, 0, streams[i]>>>(i, no_scales);
  }
}

// Main host code, a CPU thread launches all the kernels but does not wait for kernel completion,
// it returns to "work" immediately after launching all the kernels
void gpuDetectFaces(unsigned char* image, const int no_scales)
{
  checkCudaCall(cudaMemcpy2D(dev_imgs[0],(size_t)(pitch_imgs[0]*sizeof(unsigned char)), image, width_imgs[0]*sizeof(unsigned char), width_imgs[0]*sizeof(unsigned char), height_imgs[0]*NO_THREADS, cudaMemcpyHostToDevice));
  resize_image_cuda(no_scales);

  //Reset all feature extraction and classification arrays back to 0 for new batch of images
  for(int i=0;i<NO_THREADS;++i)
  {
    cudaMemset(big_pointer[i], 0, size_memory_memset_float*sizeof(float));
  }
  compute_features_cuda(no_scales);
  classify_regions_cuda(no_scales);
}

// Host code that makes sure all launched kernels have completed and transfers an array, containing
// the location of the detected faces, back to the CPU
void gpuGetFaces(float* host_faces, const int no_scales)      
{
  cudaDeviceSynchronize();
  checkCudaCall(cudaMemcpy(host_faces, big_pointer[0]+(start_pointers_vec[no_scales*(3+NO_FILTERS)]), sizeof(float)*(NO_THREADS*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)), cudaMemcpyDeviceToHost));    
}

// This function is only called once at the beginning of the program. Based on the image size
// memory is allocated for the (downscaled versions of) the image, the intermediate feature 
// representations of the image, and arrays needed for the classification and detection stages.  
void gpu_allocate_memory_constant_image_size(int height_org_img, int width_org_img, int no_scales, int cell_size, int scale_factor, int padding_rows_offset_1, int padding_cols_offset_1, int filter_rows_padding_1, int filter_cols_padding_1)
{
  #ifdef DEBUG_PRINT
  std::cout<< "ALLOCATING MEMORY FOR CONSTANT IMAGE SIZE" <<std::endl;
  #endif

  first_row = FILTER_SIZE/2;
  first_col = FILTER_SIZE/2;
  padding_rows_offset = padding_rows_offset_1;
  padding_cols_offset = padding_cols_offset_1;
  filter_rows_padding = filter_rows_padding_1;
  filter_cols_padding = filter_cols_padding_1; 
  height_imgs[0] = height_org_img;
  width_imgs[0]= width_org_img;

  for(int i=1;i<no_scales;++i)
  {
    height_imgs[i]=int(((scale_factor-1)*height_imgs[i-1])/scale_factor+0.5);
    width_imgs[i]=int(((scale_factor-1)*width_imgs[i-1])/scale_factor+0.5);
    x_scales[i] = (width_imgs[i-1]-1)/(double)std::max<float>((width_imgs[i]-1),1);
    y_scales[i] = (height_imgs[i-1]-1)/(double)std::max<float>((height_imgs[i]-1),1);
  }

  //total memory array to be memsetted
  size_memory_memset_float = 0;
  start_pointers_vec.push_back(size_memory_memset_float);

  for(int i=0;i<no_scales;++i)
  {
    height_cells_imgs[i] = (int)((float)height_imgs[i]/(float)cell_size + 0.5);
    width_cells_imgs[i] = (int)((float)width_imgs[i]/(float)cell_size + 0.5);

    height_grad_imgs[i] = std::min(height_cells_imgs[i]*cell_size, height_imgs[i])-2;
    width_grad_imgs[i] = std::min(width_cells_imgs[i]*cell_size, width_imgs[i])-2;

    height_cells_hists[i] = height_cells_imgs[i]+2;
    width_cells_hists[i] = width_cells_imgs[i]+2;

    height_cells_features[i] = height_cells_imgs[i]-2+filter_rows_padding-1;
    width_cells_features[i] = width_cells_imgs[i]-2+filter_cols_padding-1; 

    last_rows[i] = height_cells_features[i] - ((FILTER_SIZE-1)/2);
    last_cols[i] = width_cells_features[i] - ((FILTER_SIZE-1)/2);  

    cudaStreamCreate(&streams[i]);
  }

  for(int i=0;i<no_scales;++i)
  {
    size_memory_memset_float+=height_cells_hists[i]*width_cells_hists[i]*NO_BINS;
    start_pointers_vec.push_back(size_memory_memset_float);
  }

  for(int i=0;i<no_scales;++i)
  {
    size_memory_memset_float+=width_cells_imgs[i]*height_cells_imgs[i];
    start_pointers_vec.push_back(size_memory_memset_float);
  }

  for(int i=0;i<no_scales;++i)
  {
    size_memory_memset_float+=height_cells_features[i]*width_cells_features[i]*NO_FEATURES;
    start_pointers_vec.push_back(size_memory_memset_float);
  }

  for(int i=0;i<no_scales;++i)
  {
    for(int j=0;j<NO_FILTERS;++j)
    {
      size_memory_memset_float+=height_cells_features[i]*width_cells_features[i];
      start_pointers_vec.push_back(size_memory_memset_float);
    }
  }

  for(int i=0;i<NO_THREADS;++i)
  {
    size_memory_memset_float += (MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5);
    start_pointers_vec.push_back(size_memory_memset_float);
    size_memory_memset_float+=1;
    start_pointers_vec.push_back(size_memory_memset_float);
  }
  
  for(int i=0;i<no_scales;++i)
  {
    size_t pitch_img;
    checkCudaCall(cudaMallocPitch(&dev_imgs[i],&pitch_img, width_imgs[i]*sizeof(unsigned char),height_imgs[i]*NO_THREADS));   
    pitch_imgs[i] = (int)(pitch_img/sizeof(unsigned char));
  }

  for(int i=0;i<NO_THREADS;++i)
  {
    checkCudaCall(cudaMalloc(&big_pointer[i], size_memory_memset_float*sizeof(float)));
  }

  checkCudaCall(cudaMemcpyToSymbol(dev_const_imgs, &dev_imgs, NO_THREADS*MAX_NO_SCALES*sizeof(unsigned char*), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_big_pointer, &big_pointer, NO_THREADS*sizeof(float*), 0, cudaMemcpyHostToDevice));
  int* start_pointers = &start_pointers_vec[0];
  checkCudaCall(cudaMemcpyToSymbol(dev_const_start_ptrs, start_pointers, sizeof(int)*start_pointers_vec.size(), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_width_imgs, width_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_height_imgs, height_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_pitch_imgs, pitch_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_width_cells_imgs, width_cells_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_height_cells_imgs, height_cells_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_width_cells_hists, width_cells_hists, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_height_cells_hists, height_cells_hists, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_width_cells_features, width_cells_features, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_height_cells_features, height_cells_features, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_pitch_norms, pitch_norms, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_first_row, &first_row, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_first_col, &first_col, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_last_rows, last_rows, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_last_cols, last_cols, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_y_scales, y_scales, sizeof(float)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_x_scales, x_scales, sizeof(float)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_padding_rows_offset, &padding_rows_offset, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_padding_cols_offset, &padding_cols_offset, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_filter_rows_padding, &filter_rows_padding, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_filter_cols_padding, &filter_cols_padding, sizeof(int), 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_width_grad_imgs, width_grad_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_height_grad_imgs, height_grad_imgs, sizeof(int)*MAX_NO_SCALES, 0, cudaMemcpyHostToDevice));
}

// This function is called once for every rotation face detector (5 in total) at the beginning of the program. It stores the classification weights 
// of the face detection algorithm in the constant memory of the GPU. They are the same for each image and can remain
// there for the duration of the program. Also, a "look-up table" is stored in constant memory to see which "filter-weights"
// correspond to which of the 31 features. 
void store_filterbank_gpu(std::vector<std::vector<dlib::matrix < float,0,1>>> row_filters, std::vector<std::vector<dlib::matrix<float,0,1>>> col_filters, float thresh) 
{
  static int filterbank_number = 0;
  static int no_filters = 0;

  static std::vector<float> host_row_filters_vec;
  static std::vector<float> host_col_filters_vec;
  static std::vector<unsigned int> host_feature_table_vec;
  static std::vector<float> host_thresh_vec;

  static unsigned int host_cum_no_filters[5];
  host_cum_no_filters[filterbank_number]=no_filters;
  host_feature_table_vec.push_back(0);

  for (unsigned long i=0; i < row_filters.size(); ++i)
  {
    for (unsigned long j = 0; j < row_filters[i].size(); ++j)
    {
      for(int k = 0; k< row_filters[i][j].size();++k)
      {
        host_row_filters_vec.push_back(row_filters[i][j](k));
        host_col_filters_vec.push_back(col_filters[i][j](k));
      }
      no_filters++;
      //create table with feature numbers per filter 
    }
    host_feature_table_vec.push_back(row_filters[i].size()+host_feature_table_vec.back());
  }
  host_thresh_vec.push_back(thresh);

  #ifdef DEBUG_PRINT
  std::cout<<"STORE FILTERBANK GPU NUMBER: "<<filterbank_number<<std::endl;
  #endif

  if(filterbank_number<4)
  {
    filterbank_number++;
    return;
  }

  //convert vectors to arrays for cudamemcpy
  float* host_row_filters = &host_row_filters_vec[0];
  float* host_col_filters = &host_col_filters_vec[0];
  unsigned int* host_feature_table = &host_feature_table_vec[0];
  float* host_threshs = &host_thresh_vec[0];

  size_t N_features = 32*5; //
  size_t N_filters_size = no_filters*10;
  size_t N_thresh = 5;

  checkCudaCall(cudaMemcpyToSymbol(dev_const_feature_table, host_feature_table, sizeof(unsigned int)*N_features   ,0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_threshs, host_threshs, sizeof(float)*N_thresh,0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_const_col_filters, host_col_filters, sizeof(float) * N_filters_size,0, cudaMemcpyHostToDevice)); 
  checkCudaCall(cudaMemcpyToSymbol(dev_const_row_filters, host_row_filters, sizeof(float) * N_filters_size,0, cudaMemcpyHostToDevice));
  checkCudaCall(cudaMemcpyToSymbol(dev_cum_no_filters, host_cum_no_filters, sizeof(unsigned int)*5, 0, cudaMemcpyHostToDevice));

  if (dev_const_row_filters == NULL || dev_const_col_filters == NULL)
  {
    cout << "Memory allocation failed" << endl;
    checkCudaCall(cudaFree(dev_const_row_filters));
    checkCudaCall(cudaFree(dev_const_row_filters));
    exit(1);                                                    
  }
}

