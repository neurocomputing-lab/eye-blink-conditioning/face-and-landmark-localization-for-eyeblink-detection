// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

    This example program shows how to find frontal human faces in an image.  In
    particular, this program shows how you can take a list of images from the
    command line and display each on the screen with red boxes overlaid on each
    human face.

    The examples/faces folder contains some jpg images of people.  You can run
    this program on them and see the detections by executing the following command:
        ./face_detection_ex faces/*.jpg

    
    This face detector is made using the now classic Histogram of Oriented
    Gradients (HOG) feature combined with a linear classifier, an image pyramid,
    and sliding window detection scheme.  This type of object detector is fairly
    general and capable of detecting many types of semi-rigid objects in
    addition to human faces.  Therefore, if you are interested in making your
    own object detectors then read the fhog_object_detector_ex.cpp example
    program.  It shows how to use the machine learning tools which were used to
    create dlib's face detector. 


    Finally, note that the face detector is fastest when compiled with at least
    SSE2 instructions enabled.  So if you are using a PC with an Intel or AMD
    chip then you should enable at least SSE2 instructions.  If you are using
    cmake to compile this program you can enable them by using one of the
    following commands when you create the build project:
        cmake path_to_dlib_root/examples -DUSE_SSE2_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_SSE4_INSTRUCTIONS=ON
        cmake path_to_dlib_root/examples -DUSE_AVX_INSTRUCTIONS=ON
    This will set the appropriate compiler options for GCC, clang, Visual
    Studio, or the Intel compiler.  If you are using another compiler then you
    need to consult your compiler's manual to determine how to enable these
    instructions.  Note that AVX is the fastest but requires a CPU from at least
    2011.  SSE4 is the next fastest and is supported by most current machines.  
*/

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <sys/time.h>
#include "../version.h"
#include "../dlib/image_processing/box_overlap_testing.h"
#include "../dlib/image_transforms/image_pyramid.h"
#include <omp.h>
#include <math.h>

using namespace dlib;
using namespace std;

extern void initialize_profiler();
extern void end_profiler();
extern void gpuDetectFaces(unsigned char* image, const int no_scales);
extern void gpuGetFaces(float* host_faces, const int no_scales);      
extern void gpu_allocate_memory_constant_image_size(int height_org_img, int width_org_img, int no_scales, int cell_size, int scale_factor, 
    int padding_rows_offset, int padding_cols_offset, int filter_rows_padding, int filter_cols_padding);

// ----------------------------------------------------------------------------------------

void initialize_gpu_memory(frontal_face_detector detector, int width_org_img, int height_org_img, int& no_scales, dlib::pyramid_down<6>& pyr)
{
    const unsigned long min_pyramid_layer_width = detector.get_scanner().get_min_pyramid_layer_width();
    const unsigned long min_pyramid_layer_height = detector.get_scanner().get_min_pyramid_layer_height();
    const unsigned long max_pyramid_levels = detector.get_scanner().get_max_pyramid_levels();
    const unsigned long cell_size = detector.get_scanner().get_cell_size();
    const unsigned long width = detector.get_scanner().get_fhog_window_width();
    const unsigned long height = detector.get_scanner().get_fhog_window_height();
    const int padding_rows_offset = (height-1)/2;
    const int padding_cols_offset = (width-1)/2;
    no_scales = 0;  

    rectangle rect_img;
    rect_img = resize_rect(rect_img, width_org_img, height_org_img);

    double scale_factor = pyramid_rate(pyr); //0.83333
    int N = int((scale_factor)/(1-scale_factor)+1); //6 
    do
    {
        rect_img = pyr.rect_down(rect_img);
        ++no_scales;
    } while (rect_img.width() >= min_pyramid_layer_width && rect_img.height() >= min_pyramid_layer_height &&
        no_scales < max_pyramid_levels);

    gpu_allocate_memory_constant_image_size(height_org_img, width_org_img, no_scales, cell_size, N, padding_rows_offset, padding_cols_offset, height, width);
}

bool overlaps_any_box(const std::vector<rectangle>& rects, const dlib::rectangle& rect, test_box_overlap boxes_overlap)
{
    for (unsigned long i = 0; i < rects.size(); ++i)
    {
        if (boxes_overlap(rects[i], rect))
            return true;
    }
    return false;   
}

void convert_to_rects(float* host_faces, std::vector<double>& threshs, test_box_overlap boxes_overlap, 
 std::vector<rect_detection> dets_accum, std::vector<rectangle>& dets, frontal_face_detector detector, dlib::pyramid_down<6>& pyr)
{
    int no_faces = (int)host_faces[MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5];

    for(int i=0;i<no_faces;++i)
    {
        int x = int(host_faces[5*i]);
        int y = int(host_faces[5*i+1]);
        float score = host_faces[5*i+2];
        int scale = int(host_faces[5*i+3]);
        int fb = int(host_faces[5*i+4]);
        rectangle rect = detector.get_scanner().get_feature_extractor().feats_to_image(dlib::centered_rect(dlib::point(x,y),
          detector.get_scanner().get_fhog_window_width()-2*detector.get_scanner().get_padding(), detector.get_scanner().get_fhog_window_height()-2*detector.get_scanner().get_padding()),
        detector.get_scanner().get_cell_size(), detector.get_scanner().get_fhog_window_height(), detector.get_scanner().get_fhog_window_width());
        rect = pyr.rect_up(rect, scale); // CHECKEN DOET SCALE T M?
        rect_detection temp;
        temp.detection_confidence = score-threshs[fb];
        temp.weight_index = fb;
        temp.rect = rect; 
        dets_accum.push_back(temp);   
    }
    // printf("dets_accum.size():%i\n",dets_accum.size());
    dets.clear();
    std::sort(dets_accum.rbegin(), dets_accum.rend());
    for (unsigned long i = 0; i < dets_accum.size(); ++i)
    {
        if (overlaps_any_box(dets, dets_accum[i].rect, boxes_overlap))
            continue;

        dets.push_back(dets_accum[i].rect);
    }
    dets_accum.clear();
}

float euclideanDistance(dlib::point eye_one, dlib::point eye_two)
{
    return sqrt((pow((eye_one.x()-eye_two.x()),2))+(pow((eye_one.y()-eye_two.y()),2)));
}

void computeEARValue(dlib::full_object_detection& shape, std::vector<float>& ear_values)
{
    float ear_value;
    ear_value = (euclideanDistance(shape.part(44),shape.part(48)) + euclideanDistance(shape.part(45),shape.part(47)))/
    (2*euclideanDistance(shape.part(43),shape.part(46)));
    ear_values.push_back(ear_value);
}   

void plotIMG(std::vector<float>& ear_values)
{
    FILE *gnuplot = popen("gnuplot", "w");
    fprintf(gnuplot, "plot '-'\n");
    for (int i = 0; i < ear_values.size(); i++)
        fprintf(gnuplot, "%i %f\n", i, ear_values[i]);
    fprintf(gnuplot, "e\n");
    fflush(gnuplot);
    std::cin.get();
}

void combineThreadImages(array2d<unsigned char>& thread_img, unsigned char* image, char** argv, const int& no_iterations, const int& i, const int&j)
{
    load_image(thread_img, argv[1+j*no_iterations+i]);
    for(int r = 0; r<thread_img.nr(); r++)
    {
        for(int c = 0; c<thread_img.nc(); c++)
        {
            image[j*(thread_img.nc()*thread_img.nr())+r*thread_img.nc()+c] = dlib::get_pixel_intensity(thread_img[r][c]);
        }
    }
}

int main(int argc, char** argv)
{  
    try
    {
        if (argc == 1)
        {
            cout << "Give some image files as arguments to this program." << endl;
            return 0;
        }
        shape_predictor sp;
        deserialize("shape_predictor_68_face_landmarks.dat") >> sp;

        test_box_overlap boxes_overlap;
        dlib::pyramid_down<6> pyr;  

        // #pragma omp parallel for num_threads(NO_THREADS) 
        // for (int i=1;i<NO_THREADS;i++)
        // {
        //     sp[i]=sp[0];
        //     // deserialize("shape_predictor_68_face_landmarks.dat") >> sp[i];
        // }
        frontal_face_detector detector = get_frontal_face_detector();
        
        array2d<unsigned char> initial_image;
        load_image(initial_image, argv[1]);
        // pyramid_up(initial_image);

        int no_scales;
        initialize_gpu_memory(detector, initial_image.nc(), initial_image.nr(), no_scales, pyr);

        std::vector<double> threshs;
        int thresh_index = detector.get_scanner().get_num_dimensions();
        for(int fb_num=0;fb_num<NO_FILTERS;fb_num++)
        {
            double thresh = detector.get_w(fb_num)(thresh_index);
            threshs.push_back(thresh);
        }

        struct timeval startt, endt, result;
        result.tv_sec = 0;
        result.tv_usec= 0; 

        float host_faces[NO_THREADS*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)];
        std::vector<float> ear_values[NO_THREADS];
        unsigned char* image = new unsigned char[initial_image.nc()*initial_image.nr()*NO_THREADS];

        initialize_profiler();  
        printf("GO!\n");
        gettimeofday (&startt, NULL);

        bool pipeline = false;
        int no_iterations = int((argc-1)/NO_THREADS);
        printf("no_iterations:%i\n",no_iterations);
        omp_set_nested(true);

        std::vector<rect_detection> dets_accum[NO_THREADS];
        std::vector<full_object_detection> shapes[NO_THREADS];
        std::vector<rectangle> dets[NO_THREADS];
        float thread_host_faces[NO_THREADS][MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1]; 
        array2d<unsigned char> thread_img[NO_THREADS];
        array2d<unsigned char> previous_img[NO_THREADS];
        int no_faces;

        #pragma omp parallel num_threads(NO_THREADS)
        {   
            //IETS VAN ASSERT NUMTHREADS IS NUMTHREADS OF WHATEVER??? anders eruit want number of threds niet gelijk
            #pragma omp single
            {
                for(int i=0;i<no_iterations;++i)
                {
                    for(int j=0;j<NO_THREADS;j++)
                    {
                        #pragma omp task
                        {
                            combineThreadImages(thread_img[j], image, argv, no_iterations, i, j);    
                        }
                    }
                    #pragma omp taskwait
                    gpuDetectFaces(image, no_scales);
                    for(int j=0;j<NO_THREADS;j++)
                    {
                        #pragma omp task 
                        {
                            if(pipeline)
                            {
                                memcpy(thread_host_faces[j], host_faces+j*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1), (MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)*sizeof(float));
                                convert_to_rects(thread_host_faces[j], threshs, boxes_overlap, dets_accum[j], dets[j], detector, pyr);
                                //iets met gnuplot missing value als dets.size!=1 even kijken of dat kan  
                                for (unsigned long k = 0; k < dets[j].size(); ++k)
                                {
                                    dlib::full_object_detection shape = sp(previous_img[j], dets[j][k]);
                                    shapes[j].push_back(shape);
                                }
                                // computeEARValue(shapes[j][0], ear_values[j]);     
                                shapes[j].clear();
                            }
                            previous_img[j].swap(thread_img[j]);
                        }   
                    }
                    gpuGetFaces(host_faces, no_scales);
                    pipeline =true;
                }
                for(int j=0;j<NO_THREADS;j++)
                {
                    #pragma omp task                
                    {
                        memcpy(thread_host_faces[j], host_faces+j*(MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1), (MAX_NO_SCALES*NO_FILTERS*MAX_FACES*5+1)*sizeof(float));
                        convert_to_rects(thread_host_faces[j], threshs, boxes_overlap, dets_accum[j], dets[j], detector, pyr);
                        for (unsigned long k = 0; k < dets[j].size(); ++k)
                        {
                            dlib::full_object_detection shape = sp(previous_img[j], dets[j][k]);
                            shapes[j].push_back(shape);
                        }
                        // computeEARValue(shapes[j][0], ear_values[j]);     
                        shapes[j].clear();
                    }
                }
            }
        }

        end_profiler();
        gettimeofday (&endt, NULL);
        result.tv_usec += (endt.tv_sec*1000000+endt.tv_usec) - (startt.tv_sec*1000000+startt.tv_usec);

        // for(int i=1; i<NO_THREADS; ++i)
        // {
        //     ear_values[0].insert(ear_values[0].end(), ear_values[i].begin(), ear_values[i].end());
        // }
        // plotIMG(ear_values[0]);

                // printf("TIME ELAPSED LOADING IMAGES: %ld.%06ld | \n", resultimageload.tv_usec/1000000, resultimageload.tv_usec%1000000);
        printf("TIME ELAPSED TOTAL: %ld.%06ld | \n", result.tv_usec/1000000, result.tv_usec%1000000);
        printf("TIME ELAPSED PER IMAGE: %ld.%06ld | \n", (result.tv_usec/1000000)/(argc-1), (result.tv_usec%100000000)/(argc-1));
    }
catch (exception& e)
{
    cout << "\nexception thrown!" << endl;
    cout << e.what() << endl;
}
}