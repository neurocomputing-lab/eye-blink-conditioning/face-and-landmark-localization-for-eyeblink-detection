// If you want to look the the images and the face and landmark locations on each image
// #define DEBUG_PRINT

// For the allocation of constant memory on the GPU, the number of elements that need to be stored
// in constant memory must be known at compile time. However, this number is dependent on the total 
// number of scales that need to be analyzed. Therefore, here we specify the max number of scales
// that is analyzed in order to make sure we allocate enough constant memory. For REALLY big images, 
// this maximum of 25 will not be enough and needs to be increased. However, this number should probably
// not have to be changed for future implemenations
#define MAX_NO_SCALES 25 

// This parameter corresponds to the number of different detectors (for each of the five detection angles),
// it should not be changed.
#define NO_FILTERS 5

// Each classification filter has a height and width of 10 cells (80 x 80 pixels, corresponds to the detection
// window). This parameter should not be changed. 
#define FILTER_SIZE 10

// Number of features per image cell, should not be changed
#define NO_FEATURES 31

// Number of bins in each of the cell histograms, should not be changed
#define NO_BINS 18

// Maximum number of faces that can be detected per image scale per image. This should be known at compile time 
// because the array that is sent back from the CPU to the GPU with the detection locations has constant size, and 
// this parameter is used for its size. By making it large enough (a little too large mabye), we ensure that 
// the array is always large enough and we don't miss any detections.
#define MAX_FACES 50 

// Skip larger scales, allows for faster detection but not detecting the smallest faces. However, since the faces
// in this project are always minimally 20% of the image, we don't have to detect faces that are relatively smaller.
// Therefore, we can skip the largest scales (in the GPU face detection code). Note that this could be improved 
// (in cuda_face_detection_gpu.cu) by for example adjusting this so no matter what image size, only the smallest 11
// scales get analyzed. For really big images, it could be beneficial to scale down from the original image size
// to the biggest scale that needs to be analyzed at once instead of steps of 5/6th (and from that image DO scale 
// down from 5/6th). This is not needed right now but might be if in the future much larger image sizes are used. 
#define BEGIN_SCALE 1 

// This number corresponds to the number of images one which face detection is performed on the gpu, and the number
// of cpu threads used for image loading/decoding and landmark detection. This can be changed for machines with 
// a smaller number of threads
#define NO_THREADS 16 

