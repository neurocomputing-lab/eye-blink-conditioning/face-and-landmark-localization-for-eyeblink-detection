# Face and landmark localization for eyeblink detection

This work focuses on automating and accelerating the process of eyeblink detection from video in order to achieve realtime processing speeds, at frame rates ≥ 500 f ps for eye blink conditioning experiments. 

The repo contains the final versions of the files used for the thesis "Real-time face and landmark localization for eyeblink-response detection".

- The dlib folder contains the dlib library, which contains the original implementations of the face- and landmark-detection algorithms (and much more). Most of the files of interest are in the image processing and image transform folders. 

- The eyeblink_response_performance folder contains all the code required to run the accelerated eyeblink-response detection algorithm.

